<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = auth()->user();
        $giangVien = DB::table('qlsv_giangviens')
            ->where('id_user', $user->id)
            ->where('deleted_at', 0)
            ->get();
        $sinhVien = DB::table('qlsv_sinhviens')
            ->where('id_user', $user->id)
            ->where('deleted_at', 0)
            ->get();
        $phongDaoTao = DB::table('qlsv_nguoidungquantris')
            ->where('id_user', $user->id)
            ->where('deleted_at', 0)
            ->get();
        if (count($giangVien) > 0) {
            return redirect()->route('giang_vien.trangchu');
        }
        if (count($sinhVien) > 0) {
            return redirect()->route('sinh_vien.index');
        } else {
            return redirect()->route('quan_tri.trangchu');
        }
    }

    public function showChangePasswordForm(){
        return view('auth.changepassword');
    }

    public function changePassword(Request $request){

        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // Mật khẩu phù hợp
            return redirect()->back()->with("error","Mật khẩu hiện tại của bạn không khớp với mật khẩu bạn đã cung cấp. Vui lòng thử lại.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Mật khẩu hiện tại và mật khẩu mới giống nhau
            return redirect()->back()->with("error","Mật khẩu mới không được giống với mật khẩu hiện tại của bạn. Vui lòng chọn một mật khẩu khác.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:8|confirmed',
        ],

        [
            'required' => 'Không được để trống',
            'min' => 'Không được nhỏ hơn :min',
            'confirmed' => 'Xác nhận mật khẩu mới không khớp.'
        ]
    
    );

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->back()->with("success","Mật khẩu đã thay đổi thành công!");

    }
}
