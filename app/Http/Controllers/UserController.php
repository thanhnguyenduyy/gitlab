<?php

namespace App\Http\Controllers;

use App\qlsv_nhom;
use App\qlsv_vaitro;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $title = "Danh sách user";
        $users = DB::table('users')
        ->select('id','name','email')
        ->where('deleted_at',0)->get();
        return view('admin.dsuser', compact(['users', 'title']));
    }

    public function createpassword(Request $request){
        $title = "Cập nhập mật khẩu";

        return view('admin.createpassword',compact(['title']));
    }

    public function storepassword(Request $request){
        $title = "Cập nhập mật khẩu";

        if (Hash::check($request->new_password, $user->password)) { 
            $user->fill([
                 'password' => Hash::make($request->new_password)
             ])->save();

        return redirect()->back()->with('succsess','Cập nhập thành công');
        }

        return redirect()->back()->with('danger','Mật khẩu cũ không đúng');
    }

   

   
   
}
