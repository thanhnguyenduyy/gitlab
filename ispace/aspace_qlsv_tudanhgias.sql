-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: aspace
-- ------------------------------------------------------
-- Server version	5.7.32-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `qlsv_tudanhgias`
--

LOCK TABLES `qlsv_tudanhgias` WRITE;
/*!40000 ALTER TABLE `qlsv_tudanhgias` DISABLE KEYS */;
INSERT INTO `qlsv_tudanhgias` VALUES (1,1,'Về hiểu biết [Knowlegement]','Bạn biết được thêm những gì, mở rộng thêm kiến thức gì sau khi học xong môn học này','1',1,'PĐT','PĐT',0,'2021-03-26 11:42:58','2021-03-26 12:07:25'),(3,1,'Về kỹ năng thực hành công việc [S-Skill]','Bạn làm được những việc gì sau khi học xong môn học này','2',1,'PĐT','PĐT',0,'2021-03-26 11:43:43','2021-03-26 12:07:29'),(4,1,'Về thái độ [Atitute]','Qua môn học này, bạn có tích lũy được thêm điều gì về tác phong làm nghề','3',1,'PĐT','',0,'2021-03-26 11:43:53','2021-03-26 11:43:53'),(5,1,'Góp ý','Bạn có góp ý gì cho trường hoặc cảm nhận, ý kiến cá nhân của bạn về vấn đề tổ chức đào tạo tại trường?:','4',1,'PĐT','',0,'2021-03-26 11:44:03','2021-03-26 11:44:03'),(6,1,'Chấm điểm hài lòng về môn học (x/10)','Bạn chấm điểm hài lòng về môn học này','5',1,'PĐT','',0,'2021-03-26 11:45:12','2021-03-26 11:45:12');
/*!40000 ALTER TABLE `qlsv_tudanhgias` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-31 12:04:03
