-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: aspace
-- ------------------------------------------------------
-- Server version	5.7.32-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `qlsv_thongbaos`
--

LOCK TABLES `qlsv_thongbaos` WRITE;
/*!40000 ALTER TABLE `qlsv_thongbaos` DISABLE KEYS */;
INSERT INTO `qlsv_thongbaos` VALUES (1,1,'Thông báo nghỉ học','Ngày 03-10-2021 cho phép sinh viên được nghỉ học','PĐT',NULL,0,'2021-03-21 09:18:28','2021-03-21 09:18:28'),(2,1,'Thông báo nghỉ học','Ngày 03-10-2021 cho phép sinh viên được nghỉ học','PĐT',NULL,0,'2021-03-21 09:18:47','2021-03-21 09:18:47'),(3,1,'Thông báo nghỉ học','Ngày 03-10-2021 cho phép sinh viên được nghỉ học','PĐT',NULL,0,'2021-03-21 09:25:58','2021-03-21 09:25:58'),(4,1,'Thông báo nghỉ học','Ngày 03-10-2021 cho phép sinh viên được nghỉ học','PĐT',NULL,0,'2021-03-21 09:26:35','2021-03-21 09:26:35'),(5,1,'Thông báo demo','Đây là thông báo demo ngày 23-03-2021','PĐT',NULL,0,'2021-03-22 23:04:28','2021-03-22 23:04:28'),(6,20,'Thông báo cho lớp 18LT','Đây là thông báo demo','Nguyễn Bá Tuấn',NULL,0,'2021-03-22 23:08:27','2021-03-22 23:08:27'),(7,1,'1','1','PĐT',NULL,0,'2021-03-28 10:24:54','2021-03-28 10:24:54'),(8,20,'Du hành với phong vị ẩm thực mới tại Laairai','12','Nguyễn Bá Tuấn',NULL,0,'2021-03-28 10:28:22','2021-03-28 10:28:22'),(9,20,'a111111111111111111','111111111111111111111111111','Nguyễn Bá Tuấn',NULL,0,'2021-03-28 11:13:20','2021-03-28 11:13:20'),(10,20,'đây là thông báo','đây là thông báo','Nguyễn Bá Tuấn',NULL,0,'2021-03-29 01:49:23','2021-03-29 01:49:23');
/*!40000 ALTER TABLE `qlsv_thongbaos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-31 12:04:04
