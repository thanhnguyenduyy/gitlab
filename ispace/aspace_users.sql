-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: aspace
-- ------------------------------------------------------
-- Server version	5.7.32-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'PĐT','phongdaotao@gmail.com',NULL,'$2y$10$318DfK9rkmJP4jSMbKlzGurnc4JeCiqpj.Bq/t0mPVADqoilCroKC','BUHbBtmfDhhoelNyFM67rZ4PT6StiDDX3X8KIzRvmscAZqqjknXWbc4x2Aiq','2021-03-17 19:34:19','2021-03-17 19:34:19',NULL),(2,'phongdaotao1','phongdaotao1@gmail.com',NULL,'$2y$10$LqwBCbGG6neIb0S5rQp4AODxHf7ydhibfV69sbwnx0YFIVKsl9oca',NULL,'2021-03-17 19:37:30','2021-03-17 19:37:30',NULL),(3,'leduongminh','leduongminh@gmail.com',NULL,'$2y$10$MmNR8t.VF8lyXO9310Z7NOpDxPWRLPHIMU/aKSYu5FIBLLBrOrUp2',NULL,'2021-03-17 19:48:54','2021-03-17 19:48:54',NULL),(4,'maivansang','maivansanggmail.com',NULL,'$2y$10$tbSlgCqfHuxeZql3Pp8IUufjA6t7uh/x7KdJzXGTNdcaPyu63bglq',NULL,'2021-03-17 19:50:05','2021-03-17 19:50:05',NULL),(5,'trantrongy','trantrongy@gmail.com',NULL,'$2y$10$VbI3xm/AGvyriwlcogT.M..ZpwZaKw9YAdYmUmYJ0ihOzUJHJI8Iu',NULL,'2021-03-17 19:55:24','2021-03-17 19:55:24',NULL),(6,'nguyenquanghau','nguyenquanghau@gmail.com',NULL,'$2y$10$5f4tmYKvM.Ft6UlquuPLI.rdg5XXZ138412zTOUz3q4HEUpdi0dA.',NULL,'2021-03-17 19:56:20','2021-03-17 19:56:20',NULL),(7,'nguyenthitrang','nguyenthitrang@gmail.com',NULL,'$2y$10$RKSbue4WUp6.jYkQRo3rG.rTfULHNhtQGujVrsHF1c8.H6uU5JWQy',NULL,'2021-03-17 19:56:50','2021-03-17 19:56:50',NULL),(8,'nguyenduythanh','nguyenduythanh@gmail.com',NULL,'$2y$10$AYmE2dYnOxgk.lN3SsJYle.d3P6Cg37ORU2Wnq88Mhs6.xAg89.dy',NULL,'2021-03-17 19:57:41','2021-03-17 19:57:41',NULL),(9,'trandinhthanhlam','trandinhthanhlam@gmail.com',NULL,'$2y$10$lWH.AbimUAMqkIryUC.JMeJlKEnp4mDOvyZ0oZGOtBLH1tXK6XTU6',NULL,'2021-03-17 19:58:24','2021-03-17 19:58:24',NULL),(10,'nguyenquangcanh','nguyenquangcanh@gmail.com',NULL,'$2y$10$O.XJXS8Dakp1qjxEbKRXBuMtgrL076ueatqsm0/ElF3hKzbHCjKt6',NULL,'2021-03-17 19:59:07','2021-03-17 19:59:07',NULL),(11,'nguyena','nguyena@gmail.com',NULL,'$2y$10$fpCGjhWXqYYpCT6oSM9tre/uLprvjxkYdmsi8BLE5a9.Z1XG./VCW',NULL,'2021-03-17 20:01:43','2021-03-17 20:01:43',NULL),(12,'nguyenb','nguyenb@gmail.com',NULL,'$2y$10$eRbtCoaEbTFkyNNsS2tVb.fmGF3q54C07AtGXYgCYKX3hRl1/w92K',NULL,'2021-03-17 20:02:02','2021-03-17 20:02:02',NULL),(13,'nguyenc','nguyenc@gmail.com',NULL,'$2y$10$n69L4y9VjHWvHS1NpALPLuFCr6dA6XzNUKsqutr7sZvTV8YBgueUS',NULL,'2021-03-17 20:02:19','2021-03-17 20:02:19',NULL),(14,'nguyend','nguyend@gmail.com',NULL,'$2y$10$HdYmUwoNt4QoZe8G8KE.feY2V.UbRaH1nY4gGCAcZV4v8HG7FfAeK',NULL,'2021-03-17 20:02:39','2021-03-17 20:02:39',NULL),(15,'nguyene','nguyene@gmail.com',NULL,'$2y$10$LXQtEiFsAwoHWCd2dGIgnuTu8ZmW2C.S8dtoEPlY/s8wUMxVeBEOm',NULL,'2021-03-17 20:02:57','2021-03-17 20:02:57',NULL),(16,'sinhvien1','sinhvien1@gmail.com',NULL,'$2y$10$TMaboBDUR..OB41dZk65VO7h5wPKk6uRlPLAjUMiUiEZdbOei9Z0K',NULL,'2021-03-17 20:04:14','2021-03-17 20:04:14',NULL),(17,'sinhvien2','sinhvien2@gmail.com',NULL,'$2y$10$J3fYmkTbLj/NluNQJ8XxteokAfth74u0LEqpC7MCd6/fELsPytTAe',NULL,'2021-03-17 20:04:28','2021-03-17 20:04:28',NULL),(18,'sinhvien3','sinhvien3@gmail.com',NULL,'$2y$10$D/UJP0uybbcceNNEFh8gb.mjlop7Ve1aGNmgbUgKYivHGoQLfXJlu',NULL,'2021-03-17 20:04:39','2021-03-17 20:04:39',NULL),(19,'Âu Hà My','auhamy@gmail.com',NULL,'$2y$10$6qG3/hlYgmSKEmm.FlgTKuWpkDoIu7GgwKoPav1pPHJvC/nKAHVwa',NULL,'2021-03-18 03:34:45','2021-03-18 03:34:45',NULL),(20,'Nguyễn Bá Tuấn','nguyenbatuan@gmail.com',NULL,'$2y$10$TQZw8Fqe0WVnIoDTOpFaNOih6z9qEAY1CHiPYgX22kgSoRf25c93a',NULL,'2021-03-18 03:35:23','2021-03-18 03:35:23',NULL),(21,'Trần Thanh Tùng','tranthanhtung@gmail.com',NULL,'$2y$10$HzboBv.yCR2JvUF4QwQQwOAUSAChuC7qH4BIq3shO6sUBISKlUhyK',NULL,'2021-03-18 03:35:59','2021-03-18 03:35:59',NULL),(25,'Nguyễn Hoàng Y Nguyên','nguyenhoangynguyen@gmail.com',NULL,'$2y$10$.7T9hmO8H3WeKZPZT6lZ/OhyKpYsBGQN1xvWh03hqQYhnJtCi6VcG',NULL,'2021-03-18 03:39:22','2021-03-18 03:39:22',NULL),(26,'Nguyễn Gia Thọ','nguyengiatho@gmail.com',NULL,'$2y$10$E8glRq2ROE844a8JSitS6.EX6QE89mm4VRLRAyvjRQROTI1SS3nBq',NULL,'2021-03-18 03:40:53','2021-03-18 03:40:53',NULL),(27,'Mai Tuấn Nam','maituannam@gmail.com',NULL,'$2y$10$Ml9FHa1NAYD1hDlgX0ZgEu6P5.liVTVesZHGdfsdj9odRsPOgXF8a',NULL,'2021-03-18 03:41:33','2021-03-18 03:41:33',NULL),(28,'a1','a1@gmail.com',NULL,'$2y$10$rzGJ4ypW2RLP58CNqvKDg.qBLL4qdhgFJH5Oy0zziwLJjA947k1tC',NULL,'2021-03-21 10:12:58','2021-03-21 10:12:58',NULL),(29,'a12','a12@gmail.com',NULL,'$2y$10$iclUvo3L5f1F4XJ3TU4SdesuFWEFb9S0SZx8cmpFskRbkGIPAPBC.',NULL,'2021-03-21 10:13:12','2021-03-21 10:13:12',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-31 12:04:05
