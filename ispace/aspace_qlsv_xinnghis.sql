-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: aspace
-- ------------------------------------------------------
-- Server version	5.7.32-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `qlsv_xinnghis`
--

LOCK TABLES `qlsv_xinnghis` WRITE;
/*!40000 ALTER TABLE `qlsv_xinnghis` DISABLE KEYS */;
INSERT INTO `qlsv_xinnghis` VALUES (1,7,'1','1',14,'nguyenduythanh','',0,'2021-03-22 16:17:47','2021-03-22 09:17:47'),(2,3,'12','12',6,'nguyenduythanh','',0,'2021-03-22 16:18:19','2021-03-22 09:18:19'),(3,5,'xin nghỉ phép','xin nghỉ phép',1,'leduongminh','',0,'2021-03-24 05:13:33','2021-03-23 22:13:33'),(4,4,'xin nghỉ phép xin nghỉ phép','xin nghỉ phép xin nghỉ phép',8,'nguyenquangcanh','',0,'2021-03-24 05:14:06','2021-03-23 22:14:06'),(5,8,'1','1',11,'trantrongy','',0,'2021-03-24 05:18:16','2021-03-23 22:18:16'),(6,2,'xin nghỉ phép','xin nghỉ phép',3,'trantrongy','',0,'2021-03-24 05:18:39','2021-03-23 22:18:39'),(7,1,'xin nghỉ phép','xin nghỉ phép',5,'nguyenthitrang','',0,'2021-03-24 05:22:37','2021-03-23 22:22:37'),(8,8,'xin nghỉ học','bị ốm',14,'nguyenduythanh','',0,'2021-03-26 18:46:33','2021-03-26 11:46:33'),(9,6,'xin nghỉ học','bị ốm',14,'nguyenduythanh','',0,'2021-03-26 18:46:41','2021-03-26 11:46:41'),(10,1,'xin nghỉ học','bị ốm',6,'nguyenduythanh','',0,'2021-03-26 18:47:14','2021-03-26 11:47:14'),(11,5,'xin nghỉ học','bị ốm',6,'nguyenduythanh','',0,'2021-03-26 18:47:26','2021-03-26 11:47:26');
/*!40000 ALTER TABLE `qlsv_xinnghis` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-31 12:04:04
