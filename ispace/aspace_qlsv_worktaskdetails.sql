-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: aspace
-- ------------------------------------------------------
-- Server version	5.7.32-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `qlsv_worktaskdetails`
--

LOCK TABLES `qlsv_worktaskdetails` WRITE;
/*!40000 ALTER TABLE `qlsv_worktaskdetails` DISABLE KEYS */;
INSERT INTO `qlsv_worktaskdetails` VALUES (5,'Định nghĩa Model',1,'haubeo','haubeo',0,'2021-03-18 04:46:47','2021-03-18 04:46:47'),(6,'Định nghĩa View',1,'haubeo','haubeo',0,'2021-03-18 04:46:47','2021-03-18 04:46:47'),(7,'Đinh nghĩa Controller',1,'haubeo','haubeo',0,'2021-03-18 04:46:47','2021-03-18 04:46:47'),(8,'Xây dựng cấu trúc ứng dụng',1,'haubeo','haubeo',0,'2021-03-18 04:46:47','2021-03-18 04:46:47'),(9,'Định nghĩa Bootrap',2,'haubeo','haubeo',0,'2021-03-18 04:47:46','2021-03-18 04:47:46'),(10,'Đọc và phân tích URI thành MVC',2,'haubeo','haubeo',0,'2021-03-18 04:47:46','2021-03-18 04:47:46'),(11,'Chuyển hướng URI đến file PHP mong muốn',2,'haubeo','haubeo',0,'2021-03-18 04:47:46','2021-03-18 04:47:46'),(12,'Định nghĩa Router',3,'haubeo','haubeo',0,'2021-03-18 04:48:18','2021-03-18 04:48:18'),(13,'Tạo Router theo 3 cấp module, controller, action',3,'haubeo','haubeo',0,'2021-03-18 04:48:18','2021-03-18 04:48:18'),(16,'Task 1. Mô hình MVC cơ bản',4,'haubeo','haubeo',0,'2021-03-26 09:35:50','2021-03-26 09:35:50'),(17,'Task 2. Mô hình MVC cơ bản',4,'haubeo','haubeo',0,'2021-03-26 09:35:50','2021-03-26 09:35:50');
/*!40000 ALTER TABLE `qlsv_worktaskdetails` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-31 12:04:04
