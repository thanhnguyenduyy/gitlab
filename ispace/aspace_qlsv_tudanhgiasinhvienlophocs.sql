-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: aspace
-- ------------------------------------------------------
-- Server version	5.7.32-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `qlsv_tudanhgiasinhvienlophocs`
--

LOCK TABLES `qlsv_tudanhgiasinhvienlophocs` WRITE;
/*!40000 ALTER TABLE `qlsv_tudanhgiasinhvienlophocs` DISABLE KEYS */;
INSERT INTO `qlsv_tudanhgiasinhvienlophocs` VALUES (1,6,1,'Khoá 18 - PHP - Nguyễn Bá Tuấn','nguyenduythanh','',0,'2021-03-26 11:57:58','2021-03-26 04:57:58'),(2,6,3,'Khoá 18 - PHP - Nguyễn Bá Tuấn','nguyenduythanh','',0,'2021-03-26 11:57:58','2021-03-26 04:57:58'),(3,6,4,'Khoá 18 - PHP - Nguyễn Bá Tuấn','nguyenduythanh','',0,'2021-03-26 11:57:58','2021-03-26 04:57:58'),(4,6,5,'Khoá 18 - PHP - Nguyễn Bá Tuấn','nguyenduythanh','',0,'2021-03-26 11:57:58','2021-03-26 04:57:58'),(5,6,6,'Khoá 18 - PHP - Nguyễn Bá Tuấn','nguyenduythanh','',0,'2021-03-26 11:57:58','2021-03-26 04:57:58');
/*!40000 ALTER TABLE `qlsv_tudanhgiasinhvienlophocs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-31 12:04:05
