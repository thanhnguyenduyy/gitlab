-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: aspace
-- ------------------------------------------------------
-- Server version	5.7.32-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2020_12_18_075912_create_qlsv_chucnangs_table',1),(5,'2020_12_18_082114_create_qlsv_monhocs_table',1),(6,'2020_12_18_082937_create_qlsv_diemthis_table',1),(7,'2020_12_18_083514_create_qlsv_phonghocs_table',1),(8,'2020_12_18_083937_create_qlsv_menus_table',1),(9,'2020_12_18_084353_create_qlsv_nguoidungquantris_table',1),(10,'2020_12_18_085749_create_qlsv_worktasks_table',1),(11,'2020_12_18_090022_create_qlsv_worktaskdetails_table',1),(12,'2020_12_18_090247_create_qlsv_worktasksinhvienlophocs_table',1),(13,'2020_12_18_090605_create_qlsv_worktaskchitietsinhvienlophocs_table',1),(14,'2020_12_18_090828_create_qlsv_thoikhoabieus_table',1),(15,'2020_12_18_112935_qlsv_khoahoc',1),(16,'2020_12_18_115838_qlsv_giangvien',1),(17,'2020_12_18_210729_qlsv_diemdanh',1),(18,'2020_12_20_112558_qlsv_chucnangvanhom',1),(19,'2020_12_20_113016_qlsv_chucnangvauser',1),(20,'2020_12_20_113301_qlsv_sinhvien',1),(21,'2020_12_20_113845_qlsv_nhom',1),(22,'2020_12_20_114014_qlsv_kieuthi',1),(23,'2020_12_20_114219_qlsv_lophoc',1),(24,'2020_12_20_114407_qlsv_sinhvienlophoc',1),(25,'2020_12_20_114834_qlsv_tudanhgia',1),(26,'2020_12_20_115801_qlsv_tudanhgiasinhvienlophoc',1),(27,'2020_12_20_115854_qlsv_nhomvauser',1),(28,'2020_12_22_043759_qlsv_lophoc',2),(29,'2020_12_22_054807_qlsv_thoikhoabieu',3),(30,'2020_12_22_062401_qlsv_diemdanh',4),(31,'2020_12_23_081416_qlsv_thoikhoabieu',5),(32,'2020_12_24_061613_qlsv_sinhvien',6),(33,'2020_12_25_044810_qlsv_diemdanh',7),(34,'2020_12_28_052836_qlsv_sinhvienlophoc',8),(35,'2020_12_28_052927_qlsv_sinhvien',8),(36,'2021_01_03_094437_qlsv_thoikhoabieu',9),(37,'2021_01_06_102715_qlsv_diemdanh',10),(38,'2021_01_06_104001_qlsv_diemdanh',11),(39,'2021_01_06_104413_qlsv_thoikhoabieu',12),(40,'2021_01_21_173626_create_qlsv_vaitros_table',13),(41,'2021_01_21_174108_qlsv_uservavaitro',13),(42,'2021_01_21_174447_qlsv_vaitrovachucnang',14),(43,'2021_02_26_081647_create_qlsv_xinnghis_table',15),(44,'2021_02_26_153413_qlsv_xinnghis',16),(45,'2021_03_06_071543_create_qlsv_thongbaos_table',17),(46,'2021_03_06_072038_create_qlsv_thongbaonoinguoinhans_table',18),(47,'2021_03_10_080544_qlsv_xinnghis',19),(48,'2021_03_19_141243_qlsv_tudanhgiasinhvienlophocs',20),(49,'2021_03_19_165347_qlsv_xinnghis',21),(50,'2021_03_22_153348_qlsv_xinnghis',22),(51,'2021_03_22_161652_qlsv_xinnghis',23),(52,'2021_03_26_113538_qlsv_tudanhgias',24),(53,'2021_03_31_042435_qlsv_worktasksinhvienlophocs',25),(54,'2021_03_31_045251_qlsv_worktasksinhvienlophocs',26);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-31 12:04:04
